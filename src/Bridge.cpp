#include "Bridge.hpp"

#include <Logger.hpp>

#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <functional>
#include <string>
#include <thread>
#include <vector>

#include <buttplug.pb.h>
#include <errno.h>
#include <fcntl.h>
#include <google/protobuf/io/zero_copy_stream.h>
#include <google/protobuf/util/delimited_message_util.h>
#include <pthread.h>
#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <termios.h>
#include <unistd.h>

Bridge::Bridge() : _serial_fd(-1), _websock_connected(false) {
    // Set logging settings.
    _websock.set_error_channels(websocketpp::log::elevel::all);
    _websock.set_access_channels(websocketpp::log::alevel::none);

    // Initialize Asio.
    _websock.init_asio();
}

Bridge::~Bridge() {
    if (_serial_fd >= 0) {
        close(_serial_fd);
    }
}

bool Bridge::start_websocket(int port) {
    _websock.listen(port);
    _websock.set_validate_handler(
         std::bind(&Bridge::on_websocket_validate, this, std::placeholders::_1));
    _websock.set_close_handler(
         std::bind(&Bridge::on_websocket_close, this, std::placeholders::_1));
    _websock.set_message_handler(std::bind(&Bridge::on_websocket_message, this,
                                           std::placeholders::_1, std::placeholders::_2));
    logger.logd("Websocket configured on ", port);
    return true;
}

bool Bridge::start_serial(const std::string& port, int baudrate) {
    // Open port.
    _serial_fd = open(port.c_str(), O_RDWR);
    if (_serial_fd < 0) {
        logger.loge("open(", port, ") error(", errno, "): `", strerror(errno), "`.");
        return false;
    }
    else {
        logger.logd("open(", port, ")=", _serial_fd, ".");
    }

    // Configure port.
    struct termios tty_conf;
    if (tcgetattr(_serial_fd, &tty_conf) != 0) {
        logger.loge("tcgetattr(", _serial_fd, ") error(", errno, "): `", strerror(errno), "`.");
        return false;
    }

    // Disable parity.
    tty_conf.c_cflag &= ~PARENB;
    // 1 stop bit.
    tty_conf.c_cflag &= ~CSTOPB;
    // 8 bit data.
    tty_conf.c_cflag &= ~CSIZE;
    tty_conf.c_cflag |= CS8;
    // No hardware flow control.
    tty_conf.c_cflag &= ~CRTSCTS;

    tty_conf.c_cflag |= CREAD | CLOCAL;
    tty_conf.c_lflag &= ~ICANON;
    tty_conf.c_lflag &= ~ECHO;
    tty_conf.c_lflag &= ~ECHOE;
    tty_conf.c_lflag &= ~ECHONL;
    tty_conf.c_lflag &= ~ISIG;

    tty_conf.c_iflag &= ~(IXON | IXOFF | IXANY);
    tty_conf.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
    tty_conf.c_oflag &= ~OPOST;
    tty_conf.c_oflag &= ~ONLCR;

    tty_conf.c_cc[VTIME] = 0;
    tty_conf.c_cc[VMIN] = 1;

    cfsetispeed(&tty_conf, baudrate);

    if (tcsetattr(_serial_fd, TCSANOW, &tty_conf) != 0) {
        logger.loge("tcsetattr(", _serial_fd, ") error(", errno, "): `", strerror(errno), "`.");
        return false;
    }
    else {
        logger.logd("tcsetattr(", _serial_fd, ") set!");
    }

    return true;
}

bool Bridge::on_websocket_validate(websocketpp::connection_hdl hdl) {
    if (!_websock_connected) {
        logger.logi("Websocket connecting...");
        _websock_connected = true;
        _websock_hdl = hdl;
        return true;
    }
    return false;
}

void Bridge::on_websocket_close(websocketpp::connection_hdl) {
    logger.logi("Websocket disconnected.");
    _websock_connected = false;
}

void Bridge::on_websocket_message(websocketpp::connection_hdl, server_t::message_ptr msg) {
    rapidjson::Document document;
    document.Parse(msg->get_payload().c_str());

    const auto& message = document[0].GetObject();
    auto type = std::string(message.MemberBegin()->name.GetString());
    const auto& fields = message.MemberBegin()->value.GetObject();

    buttplug::Client out;

    // Ping
    if (type == "Ping") {
        auto* ping = out.mutable_ping();
        ping->Clear();
        ping->set_id(fields["Id"].GetUint());
    }
    // RequestServerInfo
    else if (type == "RequestServerInfo") {
        auto* req = out.mutable_request_server_info();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_client_name(fields["ClientName"].GetString());
        req->set_message_version(fields["MessageVersion"].GetUint());
    }
    // StartScanning
    else if (type == "StartScanning") {
        auto* req = out.mutable_start_scanning();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
    }
    // StopScanning
    else if (type == "StopScanning") {
        auto* req = out.mutable_stop_scanning();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
    }
    // RequestDeviceList
    else if (type == "RequestDeviceList") {
        auto* req = out.mutable_request_device_list();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
    }
    // RawWriteCmd
    else if (type == "RawWriteCmd") {
        auto* req = out.mutable_raw_write_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        req->set_endpoint(fields["Endpoint"].GetString());
        const auto& data = fields["Data"].GetArray();
        std::vector<uint8_t> dat;
        dat.resize(data.Size());
        for (rapidjson::SizeType i = 0; i < data.Size(); i++) {
            dat[i] = data[i].GetUint();
        }
        req->set_data(std::string((const char*) dat.data(), dat.size()));
        req->set_write_with_response(fields["WriteWithResponse"].GetBool());
    }
    // RawReadCmd
    else if (type == "RawReadCmd") {
        auto* req = out.mutable_raw_read_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        req->set_endpoint(fields["Endpoint"].GetString());
        req->set_expected_length(fields["ExpectedLength"].GetUint());
        req->set_wait_for_data(fields["WaitForData"].GetBool());
    }
    // RawSubscribeCmd
    else if (type == "RawSubscribeCmd") {
        auto* req = out.mutable_raw_subscribe_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        req->set_endpoint(fields["Endpoint"].GetString());
    }
    // RawUnsubscribeCmd
    else if (type == "RawUnsubscribeCmd") {
        auto* req = out.mutable_raw_unsubscribe_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        req->set_endpoint(fields["Endpoint"].GetString());
    }
    // StopDeviceCmd
    else if (type == "StopDeviceCmd") {
        auto* req = out.mutable_stop_device_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
    }
    // StopAllDevices
    else if (type == "StopAllDevices") {
        auto* req = out.mutable_stop_all_devices();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
    }
    // VibrateCmd
    else if (type == "VibrateCmd") {
        auto* req = out.mutable_vibrate_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        const auto& speeds_array = fields["Speeds"];
        auto* speeds_map = req->mutable_speeds();
        for (rapidjson::SizeType i = 0; i < speeds_array.Size(); i++) {
            const auto& kv = speeds_array[i];
            uint32_t k = kv["Index"].GetUint();
            float v = kv["Speed"].GetFloat();
            (*speeds_map)[k] = v;
        }
    }
    // LinearCmd
    else if (type == "LinearCmd") {
        auto* req = out.mutable_linear_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        const auto& vectors_array = fields["Vectors"];
        auto* vectors_map = req->mutable_vectors();
        for (rapidjson::SizeType i = 0; i < vectors_array.Size(); i++) {
            const auto& kv = vectors_array[i];
            uint32_t k = kv["Index"].GetUint();
            uint32_t v1 = kv["Duration"].GetUint();
            float v2 = kv["Position"].GetFloat();
            buttplug::device::generic::LinearCmd_Vectors v;
            v.Clear();
            v.set_duration(v1);
            v.set_position(v2);
            (*vectors_map)[k] = v;
        }
    }
    // RotateCmd
    else if (type == "RotateCmd") {
        auto* req = out.mutable_rotate_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
        const auto& rotations_array = fields["Rotations"];
        auto* rotations_map = req->mutable_rotations();
        for (rapidjson::SizeType i = 0; i < rotations_array.Size(); i++) {
            const auto& kv = rotations_array[i];
            uint32_t k = kv["Index"].GetUint();
            float v1 = kv["Speed"].GetFloat();
            bool v2 = kv["Clockwise"].GetBool();
            buttplug::device::generic::RotateCmd_Rotations v;
            v.Clear();
            v.set_speed(v1);
            v.set_clockwise(v2);
            (*rotations_map)[k] = v;
        }
    }
    // BatteryLevelCmd
    else if (type == "BatteryLevelCmd") {
        auto* req = out.mutable_battery_level_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
    }
    // RSSILevelCmd
    else if (type == "RSSILevelCmd") {
        auto* req = out.mutable_rssi_level_cmd();
        req->Clear();
        req->set_id(fields["Id"].GetUint());
        req->set_device_index(fields["DeviceIndex"].GetUint());
    }
    else {
        logger.logw("Could not handle client message of type: `", type, "`.");
        return;
    }

    // Write message.
    const size_t buf_size = 1024;
    uint8_t buf[buf_size];
    auto writer = google::protobuf::io::ArrayOutputStream(buf, buf_size);
    google::protobuf::util::SerializeDelimitedToZeroCopyStream(out, &writer);
    write(_serial_fd, buf, writer.ByteCount());
    logger.logd(">>", writer.ByteCount(), " ", type);
}

static void _device_pj(rapidjson::Value& root, const buttplug::enumeration::Device& dev,
                       rapidjson::Document::AllocatorType& dom_alloc) {
    rapidjson::Value device_name;
    device_name.SetString(dev.device_name().c_str(), dev.device_name().size());
    root.AddMember("DeviceName", device_name, dom_alloc);

    rapidjson::Value device_index;
    device_index.SetUint(dev.device_index());
    root.AddMember("DeviceIndex", device_index, dom_alloc);

    rapidjson::Value device_messages;
    device_messages.SetObject();
    const auto& device_messages_map = dev.device_messages();
    for (auto kv = device_messages_map.begin(); kv != device_messages_map.end(); kv++) {
        rapidjson::Value attrs;
        attrs.SetObject();
        const auto& dev_attrs = kv->second;
        if (dev_attrs.feature_count() > 0) {
            rapidjson::Value feature_count;
            feature_count.SetUint(dev_attrs.feature_count());
            attrs.AddMember("FeatureCount", feature_count, dom_alloc);
        }
        if (dev_attrs.step_count_size() > 0) {
            rapidjson::Value step_counts;
            step_counts.SetArray();
            for (int i = 0; i < dev_attrs.step_count_size(); i++) {
                rapidjson::Value v;
                v.SetUint(dev_attrs.step_count(i));
                step_counts.PushBack(v, dom_alloc);
            }
            attrs.AddMember("StepCount", step_counts, dom_alloc);
        }
        rapidjson::Value key;
        key.SetString(kv->first.c_str(), kv->first.size());
        device_messages.AddMember(key, attrs, dom_alloc);
    }
    root.AddMember("DeviceMessages", device_messages, dom_alloc);
}

void Bridge::on_serial_reader() {
    pthread_setname_np(pthread_self(), "serial");
    logger.logi("Serial reader starting.");
    // This denotes the maximum message we can decode.
    const size_t buf_size = 1024;
    uint8_t buf[buf_size];
    size_t buf_len = 0;
    buttplug::Server in;
    in.Clear();
    while (true) {
        size_t buf_read = read(_serial_fd, buf + buf_len, buf_size - buf_len);
        if (buf_read <= 0) {
            break;
        }
        else {
            buf_len += buf_read;
        }
        logger.logd("read(", _serial_fd, ")=", buf_read);

        auto reader = google::protobuf::io::ArrayInputStream(buf, buf_len);
        bool decoded =
             google::protobuf::util::ParseDelimitedFromZeroCopyStream(&in, &reader, nullptr);
        if (decoded) {
            // Consume buffer.
            size_t reader_read = reader.ByteCount();
            logger.logd("decoded(", reader_read, ")");
            buf_len -= reader_read;
            std::memmove(buf, buf + reader_read, buf_len);

            // Parse message.
            rapidjson::Document dom;
            rapidjson::Document::AllocatorType& dom_alloc = dom.GetAllocator();
            rapidjson::Value root_obj;
            root_obj.SetObject();

            if (in.has_ok()) {
                const auto& msg = in.ok();
                rapidjson::Value obj;
                obj.SetObject();
                obj.AddMember("Id", rapidjson::Value().SetUint(msg.id()), dom_alloc);
                root_obj.AddMember("Ok", obj, dom_alloc);
            }
            else if (in.has_error()) {
                const auto& msg = in.error();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value error_message;
                error_message.SetString(msg.error_message().c_str(),
                                        msg.error_message().size());
                obj.AddMember("ErrorMessage", error_message, dom_alloc);

                rapidjson::Value error_code;
                error_code.SetUint(msg.error_code());
                obj.AddMember("ErrorCode", error_code, dom_alloc);

                root_obj.AddMember("Error", obj, dom_alloc);
            }
            else if (in.has_server_info()) {
                const auto& msg = in.server_info();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value server_name;
                server_name.SetString(msg.server_name().c_str(), msg.server_name().size());
                obj.AddMember("ServerName", server_name, dom_alloc);

                rapidjson::Value message_version;
                message_version.SetUint(msg.message_version());
                obj.AddMember("MessageVersion", message_version, dom_alloc);

                rapidjson::Value max_ping_time;
                max_ping_time.SetUint(msg.max_ping_time());
                obj.AddMember("MaxPingTime", max_ping_time, dom_alloc);

                root_obj.AddMember("ServerInfo", obj, dom_alloc);
            }
            else if (in.has_scanning_finished()) {
                const auto& msg = in.scanning_finished();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                root_obj.AddMember("ScanningFinished", obj, dom_alloc);
            }
            else if (in.has_device_list()) {
                const auto& msg = in.device_list();
                rapidjson::Value obj;
                obj.SetObject();
                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value devices;
                devices.SetArray();
                for (int i = 0; i < msg.devices_size(); i++) {
                    const auto& cur_dev = msg.devices(i);
                    rapidjson::Value dev;
                    _device_pj(dev.SetObject(), cur_dev, dom_alloc);
                    devices.PushBack(dev, dom_alloc);
                }
                obj.AddMember("Devices", devices, dom_alloc);

                root_obj.AddMember("DeviceList", obj, dom_alloc);
            }
            else if (in.has_device_added()) {
                const auto& msg = in.device_added();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                _device_pj(obj, msg.device(), dom_alloc);

                root_obj.AddMember("DeviceAdded", obj, dom_alloc);
            }
            else if (in.has_device_removed()) {
                const auto& msg = in.device_removed();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value device_index;
                device_index.SetUint(msg.device_index());
                obj.AddMember("DeviceIndex", device_index, dom_alloc);

                root_obj.AddMember("DeviceRemoved", obj, dom_alloc);
            }
            else if (in.has_raw_reading()) {
                const auto& msg = in.raw_reading();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value device_index;
                device_index.SetUint(msg.device_index());
                obj.AddMember("DeviceIndex", device_index, dom_alloc);

                rapidjson::Value endpoint;
                endpoint.SetString(msg.endpoint().c_str(), msg.endpoint().size());
                obj.AddMember("Endpoint", endpoint, dom_alloc);

                rapidjson::Value data;
                data.SetArray();
                const std::string& msg_data = msg.data();
                for (size_t i = 0; i < msg_data.size(); i++) {
                    rapidjson::Value v;
                    v.SetUint(msg_data.data()[i]);
                    data.PushBack(v, dom_alloc);
                }
                obj.AddMember("Data", data, dom_alloc);

                root_obj.AddMember("RawReading", obj, dom_alloc);
            }
            else if (in.has_battery_level_reading()) {
                const auto& msg = in.battery_level_reading();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value device_index;
                device_index.SetUint(msg.device_index());
                obj.AddMember("DeviceIndex", device_index, dom_alloc);

                rapidjson::Value battery_level;
                battery_level.SetFloat(msg.battery_level());
                obj.AddMember("BatteryLevel", battery_level, dom_alloc);

                root_obj.AddMember("BatteryLevelReading", obj, dom_alloc);
            }
            else if (in.has_rssi_level_reading()) {
                const auto& msg = in.rssi_level_reading();
                rapidjson::Value obj;
                obj.SetObject();

                rapidjson::Value id;
                id.SetUint(msg.id());
                obj.AddMember("Id", id, dom_alloc);

                rapidjson::Value device_index;
                device_index.SetUint(msg.device_index());
                obj.AddMember("DeviceIndex", device_index, dom_alloc);

                rapidjson::Value rssi_level;
                rssi_level.SetInt(msg.rssi_level());
                obj.AddMember("RSSILevel", rssi_level, dom_alloc);

                root_obj.AddMember("RSSILevelReading", obj, dom_alloc);
            }
            else {
                logger.logw("decoded empty message!");
            }
            // Encode and relay message to client.
            dom.SetArray().PushBack(root_obj, dom_alloc);
            rapidjson::StringBuffer buffer;
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            dom.Accept(writer);
            const char* packet = buffer.GetString();
            if (_websock_connected) {
                _websock.send(_websock_hdl, packet, websocketpp::frame::opcode::value::text);
            }
            logger.logv(packet);
        }
        in.Clear();
    }
    logger.logi("Serial reader exiting.");
}

void Bridge::run() {
    pthread_setname_np(pthread_self(), "websock");
    std::thread serial_reader_thread(&Bridge::on_serial_reader, this);
    logger.logi("Websocket starting.");
    _websock.start_accept();
    _websock.run();
    _websock.stop();
    logger.logi("Websocket stopped.");
    serial_reader_thread.join();
}
