#include <Bridge.hpp>
#include <Logger.hpp>
#include <argparse/argparse.hpp>

#include <cstdlib>

#include <buttplug.pb.h>

int main(int argc, char* argv[]) {
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    argparse::ArgumentParser parser("buttplug bridge");

    parser.add_argument("--verbose")
         .help("increase output verbosity")
         .default_value(false)
         .implicit_value(true);

    parser.add_argument("--port")
         .help("websocket port for incoming connections")
         .default_value(12345)
         .action([](const std::string& value) { return std::stoi(value); });

    parser.add_argument("--serial")
         .help("serial port for device connection")
         .default_value(std::string("/dev/ttyACM0"));

    parser.add_argument("--rate")
         .help("serial port baudrate setting")
         .default_value(921600)
         .action([](const std::string& value) { return std::stoi(value); });

    try {
        parser.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cout << err.what() << std::endl;
        std::cout << parser;
        return EXIT_SUCCESS;
    }

    // Setup logger.
    if (parser.get<bool>("--verbose")) {
        logger.configure(true, spec::Logger::Level::ALL);
    }
    else {
        logger.configure(true, spec::Logger::Level::INFO);
    }

    // Initialize bridge.
    Bridge bridge;

    if (!bridge.start_websocket(parser.get<int>("--port"))) {
        logger.logf("Could not start websocket!");
    }
    std::string serial_port = parser.get<std::string>("--serial");
    if (!bridge.start_serial(serial_port, parser.get<int>("--rate"))) {
        logger.logf("Could not start serial!");
    }

    // Start bridge.
    bridge.run();

    // Cleanup.
    google::protobuf::ShutdownProtobufLibrary();

    return EXIT_SUCCESS;
}