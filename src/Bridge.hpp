#ifndef BTPBD_BRIDGE_HPP
#define BTPBD_BRIDGE_HPP

#include <Utils.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include <string>

class Bridge : spec::INonCopyable {
   protected:
    typedef websocketpp::server<websocketpp::config::asio> server_t;

    int _serial_fd;
    server_t _websock;
    websocketpp::connection_hdl _websock_hdl;
    bool _websock_connected;

   public:
    Bridge();
    ~Bridge();

    /**
     * Try to start listening on the specified port on the websocket protocol.
     *
     * Returns true on success and false on failure.
     */
    bool start_websocket(int port);

    /**
     * Try to open the specified serial on the specified baudrate.
     * The rest of the settings are 8 data bits, 1 stop bit,
     * no parity, no flow control.
     *
     * Returns true on success and false on failure.
     */
    bool start_serial(const std::string& port, int baudrate);

    /**
     * Start polling threads.
     */
    void run();

   protected:
    void on_serial_reader();

    bool on_websocket_validate(websocketpp::connection_hdl hdl);
    void on_websocket_close(websocketpp::connection_hdl hdl);
    void on_websocket_message(websocketpp::connection_hdl hdl, server_t::message_ptr msg);
};

#endif /*BTPBD_BRIDGE_HPP*/