/**
 * https://buttplug-spec.docs.buttplug.io/status.html
 */

syntax = "proto3";

package buttplug.status;

/**
 * Signifies that the previous message sent by the client was
 * received and processed successfully by the server.
 *
 * Server-to-Client message only.
 */
message Ok {
    /** The Id of the client message that this reply is in response to. */
    uint32 id = 1;
}

/**
 * Signifies that the previous message sent by the client
 * caused some sort of parsing or processing error on the server.
 *
 * Server-to-Client message only.
 */
message Error {
    enum Code {
        /** An unknown error occurred. */
        UNKNOWN = 0;
        /** Handshake did not succeed. */
        INIT = 1;
        /** A ping was not sent in the expected time. */
        PING = 2;
        /** A message parsing or permission error occurred. */
        MSG = 3;
        /** A command sent to a device returned an error. */
        DEVICE = 4;
    }

    /**
     * The Id of the client message that this reply is in response to,
     * assuming the Id could be parsed.
     * Id will be 0 if message could not be parsed.
     */
    uint32 id = 1;
    /** Message describing the error that happened on the server. */
    string error_message = 2;
    /** Can be used in programs to react accordingly. */
    Code error_code = 3;
}

/**
 * Ping acts a watchdog between the client and the server.
 * The server will expect the client to send a ping message
 * at a certain interval (interval will be sent to the client
 * as part of the identification step). If the client fails to
 * ping within the specified time, the server will disconnect and
 * stop all currently connected devices.
 *
 * This will handle cases like the client crashing without a proper
 * disconnect. This is not a guaranteed global failsafe,
 * since it will not guard against problems like a client UI thread
 * locking up while a client communication thread continues to work.
 *
 * Client-to-Server message with response:
 *   - Ok message with matching id on successful ping.
 *   - Error message on value or message error.
 */
message Ping {
    uint32 id = 1;
}
